package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.content.Context
import android.graphics.*
import androidx.recyclerview.widget.RecyclerView
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import kotlinx.android.synthetic.main.rvitem_list.view.*
import java.util.*
import kotlin.collections.ArrayList

class ItemHeaderListAdapter(val context: Context, private val appInterfaces: AppInterfaces,private var screenName:Enum<AppUtils.ScreenName>): RecyclerView.Adapter<ItemHeaderListAdapter.ViewHolder>() {
    private var itemHeaderList:Array<*>
    private var mainActivityViewModel:MainActivityViewModel
    init {
        itemHeaderList= Collections.emptyList<Topics>().toTypedArray()
        mainActivityViewModel= ((context as FragmentActivity?) as MainActivity).mainActivityViewModel!!
    }
    class ViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemHeaderListView = LayoutInflater.from(context).inflate(R.layout.rvitem_list,parent,false)
        return ViewHolder(itemHeaderListView)
    }

    override fun getItemCount(): Int {
        return itemHeaderList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val title:String =  AppUtils.getItemHeaderName(itemHeaderList[position])
        holder.view.itemTitle.text = title
        holder.view.setOnClickListener {
            holder.view.setBackgroundColor(Color.YELLOW)
            saveSelectedItemPositionAndItem(itemHeaderList[position],position,screenName)
            loadNextScreenByInputScreenName(screenName,appInterfaces)
        }

    }

    private fun loadNextScreenByInputScreenName(screenName: Enum<AppUtils.ScreenName>,appInterfaces: AppInterfaces) {

        when(screenName){
            AppUtils.ScreenName.TOPIC->AdObject.admob.loadNextScreen { appInterfaces.loadMenuItemlistScreen() }
            AppUtils.ScreenName.MENU->AdObject.admob.loadNextScreen { appInterfaces.loadMenuItemDetailScreen() }
            AppUtils.ScreenName.BOOKMARK->AdObject.admob.loadNextScreen { appInterfaces.loadBookMarkItemDetailScreen() }
        }


    }

    private fun saveSelectedItemPositionAndItem(selectedItem: Any?, position: Int, screenName: Enum<AppUtils.ScreenName>) {
        when(screenName){
            AppUtils.ScreenName.TOPIC->
            {
                mainActivityViewModel.topicID = (selectedItem as Topics).topicId// Save the current topic ID
                mainActivityViewModel.selectedTopic = selectedItem
            }
            AppUtils.ScreenName.MENU->
            {
                mainActivityViewModel.menuID= (selectedItem as Menus).subTypeId
                mainActivityViewModel.position=position
            }
            AppUtils.ScreenName.BOOKMARK->
            {
                mainActivityViewModel.positionBookmark=position
            }
        }
    }
    fun setDataToItemHeaderListAdapter(data: Array<*>){
        itemHeaderList=data
        notifyDataSetChanged()
    }
}

class ItemDetailPagerAdapter(private var mItems:ArrayList<*>,private val mLayoutInflater: LayoutInflater) : androidx.viewpager.widget.PagerAdapter() {


    fun setDataToPagerAdapter(data: ArrayList<*>){
        mItems=data
        notifyDataSetChanged()
    }

    override fun getItemPosition(`object`: Any): Int {
        return androidx.viewpager.widget.PagerAdapter.POSITION_NONE
    }

    override fun getCount(): Int {
        return mItems!!.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = mLayoutInflater.inflate(R.layout.vpitem_detail, container, false)
        val textView = itemView.findViewById(R.id.tvItemText) as TextView
        textView.text= getDescription(mItems!![position])
        textView.movementMethod= ScrollingMovementMethod()
        (container as androidx.viewpager.widget.ViewPager).addView(itemView)


        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    fun getDescription(inputObject: Any?): String {
        if (inputObject is Menus) return inputObject.description

        return "Invalid object class."
    }
}

/*---------------------ITEM DECORATION FOR THE RECYCLER VIEW ITEM--------------------*/
class ItemListViewDecoration(val spacing:Int = 5, val includeEdge:Boolean = true): androidx.recyclerview.widget.RecyclerView.ItemDecoration(){

    val offset = 10.0f
    var paintCyan: Paint = Paint(Color.CYAN).apply {
        style = Paint.Style.STROKE
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        // super.getItemOffsets(outRect, view, parent, state)

        if (parent.layoutManager is androidx.recyclerview.widget.GridLayoutManager) {
            val layoutManager = parent.layoutManager as androidx.recyclerview.widget.GridLayoutManager
            val spanCount = layoutManager.spanCount
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }

        }


    }

    override fun onDrawOver(c: Canvas, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        super.onDrawOver(c, parent, state)
        val lm = parent.layoutManager

        for (i in 0.until(parent.childCount)){



            val child = parent.getChildAt(i)
            val parms = child.layoutParams as androidx.recyclerview.widget.RecyclerView.LayoutParams
            val left:Float = (child.right + parms.rightMargin).toFloat()
            val right = child.left + offset
//            c!!.drawRect(
//
//                    left,child.top+offset,right,child.bottom+offset, paintCyan
//
//            )
        }
    }


}


