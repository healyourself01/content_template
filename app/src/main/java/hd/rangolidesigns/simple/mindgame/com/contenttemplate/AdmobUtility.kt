package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.util.Log
import android.widget.Toast
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.TimeUnit

class AdmobUtility(val mainActivity: androidx.fragment.app.FragmentActivity?,
                   val appInterfaces: AppInterfaces)  {
    private val mInterstitialAd = InterstitialAd(mainActivity)
    var callback: ()->Unit? = {Toast.makeText(mainActivity,"Ad failed to Load.Callback Not set.No connection.",Toast.LENGTH_SHORT).show()}
    init {
        mInterstitialAd.adUnitId= getInterstitialIDFromStrings()
        loadAdWithConnectivityCheck()
        setupInterstitialAdListeners()
    }

    private fun getInterstitialIDFromStrings() = mainActivity?.getString(R.string.INTERSTITIAL_ID)
    private fun setupInterstitialAdListeners() {
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                Toast.makeText(mainActivity, "Ad Failed to Load", Toast.LENGTH_SHORT).show()
                saveAdLoadedTime()
                 callback()
            }

            override fun onAdOpened() {
                saveAdLoadedTime()
                loadAdWithConnectivityCheck()
                callback()

            }

            override fun onAdLeftApplication() {
                loadAdWithConnectivityCheck()
                saveAdLoadedTime()
                callback()
            }

            override fun onAdClosed() {
                loadAdWithConnectivityCheck()
                callback()
            }
        }
    }
    fun loadNextScreen( cb:()->Unit){
        callback = cb
        if (isNotConnectedORNotTimedOut()){ //If the internet not avail or timer<60sec
            callback()
            return
        }
        if (isInterstitialLoadingOrLoaded()) {
            if (!showAdWithConnectivityCheck()){
                callback()} //show the ad if internet is avail only.
        } else {
            loadAdWithConnectivityCheck()
            Log.d("ERROR", "The interstitial wasn't loaded yet. Loading it now and will show it next time.")
           callback() //show the ad next time.
        }
    }
    private fun isInterstitialLoadingOrLoaded() = mInterstitialAd.isLoaded or mInterstitialAd.isLoading
    private fun isNotConnectedORNotTimedOut() = !showAdOrNot() or (AdObject.IsOnline.value == false)
    fun loadAdWithConnectivityCheck(){
        if (isConnectedAndInterstitialLoadingNotCalled()) {
            mInterstitialAd.loadAd(AdRequest.Builder().build())
        }
    }
    private fun isConnectedAndInterstitialLoadingNotCalled() =
        (AdObject.IsOnline.value == true) and (!mInterstitialAd.isLoading and !mInterstitialAd.isLoaded)
    private fun showAdWithConnectivityCheck():Boolean{
        if (mInterstitialAd.isLoaded) {
            mInterstitialAd.show()
            return true
        }
        return false
    }
    companion object {
        fun saveAdLoadedTime() {
            AdObject.TIME_LAST_LOADED = Timestamp(Date().time)
        }
        fun showAdOrNot():Boolean{
            var result:Boolean = false
            if (AdObject.TIME_LAST_LOADED == null) {
                result = true
            } else{
                //Calculate the difference in seconds
                val diff = TimeUnit.MILLISECONDS.toSeconds (Timestamp(Date().time).time - AdObject.TIME_LAST_LOADED!!.time)
                if (diff >= AdObject.TIME_INTERVAL_AD){
                    result = true
                }     }

            return result

        }


    }
}