package hd.rangolidesigns.simple.mindgame.com.contenttemplate

interface AppInterfaces {
    fun loadMenuItemDetailScreen()
    fun loadStartScreen()
    fun loadTopicsScreen()
    fun loadMenuItemlistScreen()
    fun loadBookmarkItemlistScreen()
    fun loadBookMarkItemDetailScreen()
    fun loadPrivacyPolicy()


}