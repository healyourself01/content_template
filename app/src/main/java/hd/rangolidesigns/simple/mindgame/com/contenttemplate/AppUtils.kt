package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.core.content.FileProvider
import android.widget.Toast
import android.app.Activity
import android.net.Uri
import android.os.AsyncTask
import androidx.core.app.ShareCompat
import android.util.Log
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.snackbar.Snackbar
import java.io.*


object  AppUtils {

     fun shareImage(bmp: Bitmap,ctx:Context) {

        val share = Intent(Intent.ACTION_SEND)
         share.putExtra(Intent.EXTRA_TEXT, "Please Download the app")
         share.putExtra(Intent.EXTRA_TEXT, "Hey please check this application \n https://play.google.com/store/apps/details?id="+AdObject.PACKAGE_NAME)
         share.type = "image/*"

        val bytes = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

        val f = File(ItemDataset.APP_DIR,"temp.jpg")
         val uriShare = FileProvider.getUriForFile(ctx,
                 AdObject.PACKAGE_NAME + ".fileprovider", f)

        try {
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
        } catch (e: IOException) {
            e.printStackTrace()
        }

        share.putExtra(Intent.EXTRA_STREAM, uriShare)
        try {
            ctx.startActivity(Intent.createChooser(share, "Share via"))
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(ctx, "Please Connect To Internet", Toast.LENGTH_LONG)
                    .show()
        }

    }

    fun shareText(name:String,description:String, ctx:Activity){


        val referalText="Hey please check this application \n https://play.google.com/store/apps/details?id="+AdObject.PACKAGE_NAME
        val shareText= "$name\n$description\n \n$referalText"

        val shareIntent = ShareCompat.IntentBuilder.from(ctx)
            .setType("text/plain")
            .setText(shareText)
            .intent

        if (shareIntent.resolveActivity(ctx.packageManager) != null) {
            ctx.startActivity(shareIntent)
        }
    }

    fun copyDbFromAssetsToApp(context: Context,assetsDbFileName: String,appDbName:String){
        val appDbFile =File(context.getDatabasePath(appDbName).absolutePath)
        val dbDirectory=File(context.getDatabasePath(appDbName).parent)
        if(!dbDirectory.isDirectory)  dbDirectory.mkdirs()
        if (!appDbFile.exists()){
            context.assets.open(assetsDbFileName).copyTo(appDbFile.outputStream())
        }
    }

    /*load Image into the image view with glide*/
    fun loadImageFromAssets(itemImageNameinAsset: String?, activity: androidx.fragment.app.FragmentActivity, imageViewHolder:ImageView){
        if (!itemImageNameinAsset.isNullOrBlank()) {
            val iconPath=ItemDataset.ASSET_URI+itemImageNameinAsset+".jpg"
            try {
                GlideApp.with(activity)
                    .load(Uri.parse(iconPath))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageViewHolder)
            } catch (e: Exception) {
                Log.e("error",iconPath+" : "+e.message)
            }
        }else{
            try {
                GlideApp.with(activity)
                    .load(R.drawable.app_banner)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageViewHolder)
            } catch (e: Exception) {
                Log.e("Glide-Error", e.message)
            }

        }

    }

    fun getItemHeaderName(item: Any?): String {

        if (item is Topics){
            return item.topicName
        }
        if (item is Menus){
            return item.name
        }

        return "Error in Item Name"
    }

    enum class ScreenName {
    TOPIC,MENU,BOOKMARK
}

    fun saveImage(b: Bitmap, img_name:String,activity: FragmentActivity) {


        val fullPath =  ItemDataset.APP_DIR
        try {
            val dir = fullPath
            if (!dir.exists()) {
                dir.mkdirs()
            }
            var fOut: OutputStream? = null
            val file = File(fullPath, "$img_name")
            if (file.exists())
                file.delete()
            file.createNewFile()
            fOut = FileOutputStream(file)
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.WEBP, 100, fOut)
            fOut.flush()
            fOut.close()
//            Toast.makeText(activity,"Image $img_name saved successfully.",Toast.LENGTH_SHORT).show()
            Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Image $img_name saved successfully.", Snackbar.LENGTH_SHORT).show()

        } catch (e: Exception) {
            Log.e("EXCEPTION", e.message)
//            Toast.makeText(activity,"Error in saving Image $img_name.",Toast.LENGTH_SHORT).show()
            Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Error in saving Image $img_name.", Snackbar.LENGTH_SHORT).show()
        }

    }

    class RunCodeAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            handler()
            return null
        }
    }
}