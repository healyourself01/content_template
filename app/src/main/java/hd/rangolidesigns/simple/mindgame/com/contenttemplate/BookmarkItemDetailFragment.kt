package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_item_detail.view.*

class BookmarkItemDetailFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        getViewModelFromMainActivity()
        val bookmarkItemDetailView = inflateViewFromXML(inflater, container)
        mAllBookMarks=mainActivityViewModel.allBookMarks.toCollection(ArrayList())
        appCompatActivity = activity as AppCompatActivity
        initializeViewObjHolderVariables(bookmarkItemDetailView!!)
        setupActionBar(bookmarkItemDetailView)
        setupViewpagerAndCollapsingToolbar()
        return bookmarkItemDetailView

    }

    private fun getViewModelFromMainActivity() {
        mainActivityViewModel = ViewModelProviders.of(activity as FragmentActivity).get(MainActivityViewModel::class.java)
    }
    private fun inflateViewFromXML(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View? {
        mLayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.fragment_item_detail, container, false)
    }
    private fun initializeViewObjHolderVariables(v: View) {
        toolbar = v.toolbar_holder
        collapsingToolbarLayout = v.collapsing_toolbar_layout
        itemDetailTextAdapter = v.vpItemDetailText
        itemToolbarImage = v.ivItemToolBarImage
    }
    private fun setupActionBar(bookmarkItemDetailView: View?) {
        appCompatActivity.setSupportActionBar(bookmarkItemDetailView!!.toolbar_holder)
        setHasOptionsMenu(true)
    }
    private fun setupViewpagerAndCollapsingToolbar() {
        setupViewPager()
        if (mAllBookMarks.isEmpty()) getDataFromDB()
        else {
            setupCollapsingToolBar()
            setupPageChangeListeners()
        }
    }

    private fun getDataFromDB() {
        mainActivityViewModel.getAllBookmarks()
            .observe(activity as LifecycleOwner, Observer { menusList ->
                mAllBookMarks = menusList.toCollection(ArrayList())
                (itemDetailTextAdapter.adapter as ItemDetailPagerAdapter).setDataToPagerAdapter(mAllBookMarks)
                setupCollapsingToolBar()
            })

    }

    private fun setupCollapsingToolBar() {
        itemDetailTextAdapter.currentItem = getSavedPosition()
        collapsingToolbarLayout.title = mAllBookMarks[getSavedPosition()].name
        AppUtils.loadImageFromAssets(mAllBookMarks[getSavedPosition()].image, appCompatActivity, itemToolbarImage)
    }

    private fun setupViewPager() {
        itemDetailTextAdapter.adapter =
                ItemDetailPagerAdapter(mItems = mAllBookMarks, mLayoutInflater = mLayoutInflater)
    }
    private fun setupPageChangeListeners() {
        itemDetailTextAdapter.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                updateBookmarkPosition(position)
                showInterstitialAd()
            }
            override fun onPageScrollStateChanged(state: Int) {
                loadNextItemFromViewpagerAdapter(appCompatActivity)
            }
        })
    }
    private fun showInterstitialAd() {
        AdObject.admob.loadNextScreen { }
    }
    private fun loadNextItemFromViewpagerAdapter(activity: AppCompatActivity) {
        updateBookmarkPosition(itemDetailTextAdapter.currentItem)
        collapsingToolbarLayout.title = mAllBookMarks[itemDetailTextAdapter.currentItem].name
        AppUtils.loadImageFromAssets(
            mAllBookMarks[itemDetailTextAdapter.currentItem].image,
            activity,
            imageViewHolder = itemToolbarImage
        )
    }
    private fun updateBookmarkPosition(position: Int) {
        mainActivityViewModel.positionBookmark = position
    }

    private fun getSavedPosition() = mainActivityViewModel.positionBookmark

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_bookmarks, menu)
        setHasOptionsMenu(true)
        super.onCreateOptionsMenu(menu, inflater)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val itemSelected = mAllBookMarks.elementAt(getPagerCurrentPosition())
        when (item.itemId) {
        /*---SHARE THE ITEM-----------*/
            R.id.btnShare -> {
                AppUtils.shareText(itemSelected.name,itemSelected.description,activity as Activity)
            }
      /*---DELETE THE ITEM-----------*/
            R.id.btnDelete -> {
                val currentItem=  mAllBookMarks[getPagerCurrentPosition()]
                mainActivityViewModel.deleteBookMark(currentItem.id)
                mAllBookMarks.removeAt(getPagerCurrentPosition())
                Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Deleted ${currentItem.name} Successfully.", Snackbar.LENGTH_SHORT).show()
                if (mAllBookMarks.isNotEmpty()) {
                    itemDetailTextAdapter.adapter!!.notifyDataSetChanged()
                    updateBookmarkPosition(getPagerCurrentPosition())
                    collapsingToolbarLayout.title= mAllBookMarks[getPagerCurrentPosition()].name
                    AppUtils.loadImageFromAssets(mAllBookMarks[getPagerCurrentPosition()].image,activity as androidx.fragment.app.FragmentActivity,imageViewHolder = itemToolbarImage)
                } else {
                    Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "All Bookmarks deleted.Going back to Item Detail page.", Snackbar.LENGTH_SHORT).show()
                    AdObject.fragmentsStack.pop() //remove the entry for - BookmarkItemDetailFragment
                    AdObject.fragmentsStack.pop() //remove the entry for - BookMarkFragment
                    appInterfaces.loadMenuItemDetailScreen() //Load the Image Menus
                }
            }


        /*--LOAD THE BOOK MARKS SCREEN-----*/
            R.id.btnBookMarksList->{
                AdObject.admob.loadNextScreen {
                   appInterfaces.loadBookmarkItemlistScreen()
                }
            }

            R.id.btnRate->{
            AdObject.rateApp(activity as Context)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getPagerCurrentPosition() = itemDetailTextAdapter.currentItem
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppInterfaces){ appInterfaces = context }
    }

    lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private lateinit var mainActivityViewModel: MainActivityViewModel
    private lateinit var collapsingToolbarLayout: CollapsingToolbarLayout
    private lateinit var itemDetailTextAdapter: androidx.viewpager.widget.ViewPager
    private lateinit var itemToolbarImage:ImageView
    private lateinit var appInterfaces:AppInterfaces
    private lateinit var mLayoutInflater: LayoutInflater
    private lateinit var mAllBookMarks:ArrayList<Menus>
    private lateinit var appCompatActivity: AppCompatActivity

}
