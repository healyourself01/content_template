package hd.rangolidesigns.simple.mindgame.com.contenttemplate



import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_header.view.*


class BookmarkItemListFragment : androidx.fragment.app.Fragment() {
    private lateinit var appInterfaces:AppInterfaces
    lateinit var itemListRecyclerView: RecyclerView
    private lateinit var mainActivityViewModel:MainActivityViewModel
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppInterfaces){ appInterfaces = context }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        getMainActivityViewModel()
//        mainActivityViewModel=  (activity as MainActivity).mainActivityViewModel!!
        val bookmarkListView: View = inflateFragmentXMLToView(inflater, container)
        setupRecyclerView(bookmarkListView)
        setDataToAdapter(bookmarkListView)
        return bookmarkListView
    }

    private fun getMainActivityViewModel() {
        mainActivityViewModel = ViewModelProviders.of(activity as FragmentActivity).get(MainActivityViewModel::class.java)
    }


    private fun inflateFragmentXMLToView( inflater: LayoutInflater,container: ViewGroup?): View {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    private fun setupRecyclerView(menuFragmentView: View) {
        menuFragmentView.pageHeader.text = "Menus loading.."
        itemListRecyclerView=menuFragmentView.findViewById(R.id.itemListRecyclerView)
        itemListRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ItemHeaderListAdapter(context, appInterfaces,AppUtils.ScreenName.BOOKMARK)
            addItemDecoration(ItemListViewDecoration(spacing = 10, includeEdge = false))
        }
    }
    private fun setDataToAdapter(topicFragmentView: View) {
        mainActivityViewModel.getAllBookmarks().observe(activity as LifecycleOwner, Observer {bookmarksList->
            mainActivityViewModel.allBookMarks = bookmarksList
            topicFragmentView.pageHeader.text = "${bookmarksList.size} Menus loaded"
            (itemListRecyclerView.adapter as ItemHeaderListAdapter).setDataToItemHeaderListAdapter(bookmarksList)
        })
    }



}
