package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQuery
import androidx.room.*
import android.content.Context
import android.os.AsyncTask


/*----------------------------TABLE SCHEMA CLASSES------------------------------*/
@Entity(tableName = "TOPICS")
class Topics(

        @field:PrimaryKey
        @field:ColumnInfo(name = "TOPIC_ID")
        val topicId:Int,
        @field:ColumnInfo(name = "TOPIC_NAME")
        val topicName:String,
        @field:ColumnInfo(name = "STATUS")
        val status:String="1",
        @field:ColumnInfo(name = "IMAGE_NAME")
        val imageName:String?=""

)

@Entity(tableName = "MENUS")
class Menus(
        @field:PrimaryKey
        @field:ColumnInfo(name = "ID")
        var id:Int=0,
        @field:ColumnInfo(name = "TYPE_ID")
        var typeId:Int=0,
        @field:ColumnInfo(name = "SUB_TYPE_ID")
        var subTypeId:Int=0,
        @field:ColumnInfo(name = "NAME")
        var name:String="",
        @field:ColumnInfo(name = "DESCRIPTION")
        var description:String="",
        @field:ColumnInfo(name = "DESCRIPTION_ADD")
        var descriptionAdd:String?="",
        @field:ColumnInfo(name = "IMAGE")
        var image:String?="",
        @field:ColumnInfo(name = "STATUS")
        var status:String?="",
        @field:ColumnInfo(name="BOOKMARK_STATUS")
        var bookmarkStatus:String?=""
){
    constructor() : this(0,0,0,"","","","","")
}
/*----------------------------DAO INTERFACE------------------------------*/
@Dao
interface AppDao{
    /*ALL THE LIST OF TOPICS*/
    @get:Query("SELECT * FROM TOPICS ORDER BY TOPIC_ID")
    val allTopics:LiveData<Array<Topics>>

    /*Get the list of menus for the given topic*/
    @Query("SELECT * FROM MENUS WHERE TYPE_ID=:topicId ORDER BY TYPE_ID,SUB_TYPE_ID")
    fun getMenus(topicId:Int?):LiveData<Array<Menus>>
    /*--------------------------BOOKMARK QUERIES-----------------------------*/
    @Query("UPDATE MENUS SET BOOKMARK_STATUS='1' WHERE ID =:rowId")
    fun addBookMark(rowId:Int)

    @Query("UPDATE MENUS SET BOOKMARK_STATUS='' WHERE ID =:rowId")
    fun deleteBookMark(rowId: Int)

    @Query("SELECT * FROM MENUS WHERE BOOKMARK_STATUS='1' ORDER BY TYPE_ID,SUB_TYPE_ID")
    fun getAllBookMarks():LiveData<Array<Menus>>
    /*--------------------------BOOKMARK QUERIES-----------------------------*/
    @RawQuery
    fun rawQuery(qry:SupportSQLiteQuery):Array<String>

    /*Get the ITEM TEXT for the given topic,menu*/
    @Query("SELECT DESCRIPTION FROM MENUS WHERE TYPE_ID=:topicId and SUB_TYPE_ID=:menuId")
    fun getItem(topicId: Int?,menuId:Int?):LiveData<String>


}
/*--------------------------------------------DATABASE-----------------------------------------------*/
@Database(entities = [Topics::class,Menus::class],version = 2)
abstract class AppRoomDatabase:RoomDatabase(){
    abstract fun appDao():AppDao
    lateinit var ctx:Context
    /**
     * Populate the database in the background.
     * If you want to start with more words, just add them.
     */
    private class PopulateDbAsync internal constructor(db: AppRoomDatabase,ctx: Context) : AsyncTask<Void, Void, Void>() {

        private val mDao: AppDao

        init {
            mDao = db.appDao()
        }

        override fun doInBackground(vararg params: Void): Void? {
            return null
        }
    }

    companion object {
        // marking the instance as volatile to ensure atomic access to the variable
        @Volatile
        private var INSTANCE: AppRoomDatabase? = null

        internal fun getDatabase(context: Context): AppRoomDatabase? {
            if (INSTANCE == null) {
                synchronized(lock = AppRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                AppRoomDatabase::class.java, AdObject.DB_NAME)
                                // Wipes and rebuilds instead of migrating if no Migration object.
                                // Migration is not part of this codelab.
                                .fallbackToDestructiveMigration()
//                                .allowMainThreadQueries()
//                                .addCallback(OnDBOpenCallback(context))
                                .build()
                    }
                }
            }
            return INSTANCE
        }

        /**
         * Override the onOpen method to populate the database.
         * For this sample, we clear the database every time it is created or opened.
         *
         * If you want to populate the database only when the database is created for the 1st time,
         * override RoomDatabase.Callback()#onCreate
         */

        private class OnDBOpenCallback(val ctx:Context):RoomDatabase.Callback(){
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                PopulateDbAsync(INSTANCE!!,ctx).execute()
            }
        }


    }
}
/*---------------------------------DATABASE REPOSITORY------------------------------------------------------------*/
class AppDBRepository(application:Application){
    private var appDao: AppDao
    var allTopics: LiveData<Array<Topics>>
    var allBookMarks: LiveData<Array<Menus>>

    init {
        val db:AppRoomDatabase= AppRoomDatabase.getDatabase(application)!!
        appDao=db.appDao()
        allTopics= appDao.allTopics
        allBookMarks=appDao.getAllBookMarks()
    }
    fun getMenus(rowId: Int): LiveData<Array<Menus>> { return appDao.getMenus(rowId)}
    fun addBookMark(rowId: Int){appDao.addBookMark(rowId = rowId)}
    fun deleteBookMark(rowId: Int){appDao.deleteBookMark(rowId = rowId)}
}