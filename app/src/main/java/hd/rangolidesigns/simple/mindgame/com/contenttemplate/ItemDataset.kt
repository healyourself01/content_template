package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.os.AsyncTask
import androidx.lifecycle.LifecycleOwner
import java.io.File


object ItemDataset {
    var position: Int = 0
    var position_bookmark: Int = 0
    lateinit var APP_DIR: File

    lateinit var items: Collection<Item>
    var TOPIC_ID: Int=1
    var MENU_ID: Int=1
    lateinit var admob: AdmobUtility
    const val ASSET_URI = "file:///android_asset/app_images/"
    var dbRepository: AppRoomDatabase?=null

    lateinit var allTopics: Array<Topics>
    var allBookmarks: ArrayList<Menus>? = arrayListOf()
    lateinit var selectedTopic:Topics
    lateinit var selectedMenus: Array<Menus>

    init {

    }

fun addBookmark(rowId: Int): Boolean {
    AppUtils.RunCodeAsync{
    dbRepository!!.appDao().addBookMark(rowId = rowId)
    }.execute()
    return true
}

fun deleteBookmark(rowId: Int):Boolean{
    AppUtils.RunCodeAsync{
    dbRepository!!.appDao().deleteBookMark(rowId = rowId)
    }.execute()
    return true
}


}

