package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import java.io.File


class MainActivity : AppCompatActivity(), AppInterfaces {

    private val START_SCREEN = "START_SCREEN"
    private val TOPICS = "TOPICS"
    private val MENUS = "MENUS"
    private val ITEM = "ITEM"
    private val BOOKMARK_MENU = "BOOKMARK_MENU"
    private val BOOKMARK_ITEM = "BOOKMARK_ITEM"
    private var MSG_DISPLAYED = false

    private var BANNER_LOADED = false
    private var DB_NAME = ""
    private lateinit var assetsDbFileToBeCopied:String
    var mainActivityViewModel: MainActivityViewModel?=null


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startInternetMonitoringService()
        initializeDatabase()
        initializeAdobjectItemDataset()
        setupCustomFontForTextviewFromAssets()
        setupOfflineMessageObserver()
        loadBannerWithConnectivityCheck()
        loadAppScreenFragment()

        Thread.setDefaultUncaughtExceptionHandler { t, e -> System.err.println(e.printStackTrace()) }

    }

    private fun loadAppScreenFragment() {

            if (AdObject.FRAGMENT_LOADED) {
                val prevScreen:String = AdObject.fragmentsStack.peek() as String
                loadPreviousScreen(prevScreen = prevScreen)
            } else {
                loadStartScreen()
            }
    }

    private fun setupOfflineMessageObserver() {
        AdObject.IsOnline.observeForever {
            if ((it == false) and (MSG_DISPLAYED == false)) {
                Snackbar.make(clMainActivity, "You are Offline.Please check internet connection.", Snackbar.LENGTH_SHORT).show()
                MSG_DISPLAYED = true
            } else if (it == true) MSG_DISPLAYED = false
        }
    }

    private fun initializeAdobjectItemDataset() {
        AdObject.connectivityManager =
                applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        AdObject.INTERSTITIAL_ID = AdObject.INTERSTITIAL_TEST_ID
        ItemDataset.APP_DIR = File(filesDir, "bookmarks")
        ItemDataset.APP_DIR.mkdirs()
        AdObject.PACKAGE_NAME = packageName
        AdObject.admob= AdmobUtility(this,appInterfaces = this as AppInterfaces)
    }

    private fun initializeDatabase() {
        DB_NAME = packageName.toString().replace(".", "_")+".db"
        AdObject.DB_NAME = DB_NAME
        /*--------------------------DATABASE SETUP-----------------------------------------*/
        copyDbFromAssets()
        createRoomDatabase()
    }


    private fun createRoomDatabase() {
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    private fun copyDbFromAssets() {
        assetsDbFileToBeCopied=getString(R.string.ASSETS_DB_TO_BE_COPIED)
        AppUtils.copyDbFromAssetsToApp(context = this, assetsDbFileName = assetsDbFileToBeCopied, appDbName = DB_NAME)
    }

    private fun startInternetMonitoringService() {
        /*---------------SERVICE TO CHECK INTERNET CONNECTION----------------------*/
        Worker.enqueueWork(this)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        Log.d("Awww", "App in background")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        Log.d("Yeeey", "App in foreground")
    }

    /*----------------------ON BACK PRESS FOR THE ACTIVITY AND FRAGMENTS-------------------*/
    override fun onBackPressed() {
        if (AdObject.fragmentsStack.size > 1) {
            AdObject.fragmentsStack.pop()
            val prevScreen = AdObject.fragmentsStack.pop() as String
            loadPreviousScreen(prevScreen)
        } else {
            AdObject.FRAGMENT_LOADED = false /*WHEN APP IS GOING INTO BACKGROUND, SET FRAGMENT_LOADED = FALSE*/
            AdObject.SPLASH_CALLED = false
            super.onBackPressed()
        }
     }

    private fun loadPreviousScreen(prevScreen: String) {
        when (prevScreen) {
            START_SCREEN -> {
                loadStartScreen()
            }
            TOPICS -> {
                loadTopicsScreen()
            }
            MENUS -> loadMenuItemlistScreen()
            ITEM -> loadMenuItemDetailScreen()
            BOOKMARK_ITEM -> loadBookMarkItemDetailScreen()
            BOOKMARK_MENU -> loadBookmarkItemlistScreen()
        }
    }

    /*--------------------------------------------SCREEN LOADING VIA FRAGMENTS--------------------------------------------------------*/

    private fun loadFragmentToActivity(fragmentScreen:Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_holder, fragmentScreen)
            commitAllowingStateLoss()
        }
    }

    /*-----------------------SCREEN 0 - THE MAIN SCREEN----------------------*/
    override fun loadStartScreen() {
        if (!isFinishing) {
            loadFragmentToActivity(StartScreenFragment())
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(START_SCREEN)
            loadBannerWithConnectivityCheck()
        }
    }

    override fun loadPrivacyPolicy() {
        startActivity(Intent(this, PrivacyPolicy::class.java))
    }

    /*-----------------------SCREEN 1 - THE IMAGE TOPICS----------------------*/
    override fun loadTopicsScreen() {
        if (!isFinishing) {
            loadFragmentToActivity(TopicFragment())
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(TOPICS)
            loadBannerWithConnectivityCheck()
        }
    }

    /*---------------------SCREEN 2 - IMAGE MENUS---------------------*/
    override fun loadMenuItemlistScreen() {
        if (!isFinishing) {
            loadFragmentToActivity(MenuItemListFragment())
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(MENUS)
            loadBannerWithConnectivityCheck()
        }
    }

    /*------------------------------SCREEN 3 - THE IMAGE ITEM------------------------*/
    override fun loadMenuItemDetailScreen() {
        if (!isFinishing) {
            loadFragmentToActivity(MenuItemDetailFragment())
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(ITEM)
            loadBannerWithConnectivityCheck()
        }
    }

    /*------------------------------SCREEN 4 - THE BOOK MARK MENU------------------------*/
    override fun loadBookmarkItemlistScreen() {
        if (!isFinishing) {
            loadFragmentToActivity(BookmarkItemListFragment())
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(BOOKMARK_MENU)
            loadBannerWithConnectivityCheck()
        }
    }

    /*------------------------------SCREEN 5 - THE BOOK MARK ITEM------------------------*/
    override fun loadBookMarkItemDetailScreen() {
        if (!isFinishing) {
            loadFragmentToActivity(BookmarkItemDetailFragment())
            AdObject.FRAGMENT_LOADED = true
            AdObject.fragmentsStack.push(BOOKMARK_ITEM)
            loadBannerWithConnectivityCheck()
        }
    }

/*--------------------------------------------SCREEN LOADING VIA FRAGMENTS--------------------------------------------------------*/

    fun setupCustomFontForTextviewFromAssets(){
        val fontPath="file:///android_asset/fonts/"+AdObject.FONT_NAME
        copyFontFromAssets()
        //This logic not working properly -need to fix
//        if (File(fontPath).isFile) {
//            copyFontFromAssets()
//        }
//        else {
//            Snackbar.make(clMainActivity,"Font ${AdObject.FONT_NAME} not available in Assets/fonts!",Snackbar.LENGTH_SHORT).show()
//        }
    }

    private fun copyFontFromAssets() {
        AdObject.typeface = Typeface.createFromAsset(assets, "fonts/" + AdObject.FONT_NAME)
    }

    fun parseJSON(array: String): Boolean {

        doAsync {
            val jsonArray = streamingArray(array)

        }
        runOnUiThread {
            alert("JSON parsing done!") {
                yesButton {
                    toast("Yat")
                    noButton { toast("haha") }
                }
            }
        }
        return true
    }


    fun streamingArray(array: String): Collection<Item> {


        val gson = Gson()

        // Deserialization
        val collectionType = object : TypeToken<Collection<Item>>() {}.type
        val result = gson.fromJson<Collection<Item>>(array, collectionType)


//
//        }
        return result
    }

    private fun loadBannerWithConnectivityCheck() {
        if (AdObject.isConnected() and !adBanner.isLoading and !BANNER_LOADED) {


            val adRequest = AdRequest.Builder().build()
            adBanner.loadAd(adRequest)

            adBanner.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                    // Toast.makeText(applicationContext, "Banner Loaded", Toast.LENGTH_SHORT).show()
//                    Snackbar.make(clMainActivity, "Banner Loaded", Snackbar.LENGTH_SHORT).show()
                    BANNER_LOADED = true
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    // Code to be executed when an ad request fails.
                    // Toast.makeText(applicationContext, "Banner Failed:" + errorCode.toString(), Toast.LENGTH_SHORT).show()
                    Snackbar.make(clMainActivity, "Banner Failed:" + errorCode.toString(), Snackbar.LENGTH_SHORT).show()
                }

                override fun onAdOpened() {
                    // Code to be executed when an ad opens an overlay that
                    // covers the screen.
                }

                override fun onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                }

                override fun onAdClosed() {
                    // Code to be executed when when the user is about to return
                    // to the app after tapping on an ad.
                }
            }

        }
    }

    /*--------------TO RESTORE THE SCREEN STATE ON RESUME---------------*/
    override fun onResume() {
        super.onResume()
        loadAppScreenFragment()
     }

}


