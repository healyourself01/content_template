package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {

    private var appDbRepository: AppDBRepository
    var allTopics:LiveData<Array<Topics>>

    var allBookMarks: Array<Menus> =arrayOf()
    var selectedTopic:Topics = Topics(0,"","","")
    var selectedMenus: Array<Menus> = arrayOf()
    lateinit var admob: AdmobUtility

    var topicID: Int=0
    var menuID: Int=0
    var position:Int=0
    var positionBookmark:Int=0

    init {
        appDbRepository=AppDBRepository(application)
        allTopics=appDbRepository.allTopics
    }
    fun getMenus(rowId: Int): LiveData<Array<Menus>> { return appDbRepository.getMenus(rowId)}
    fun getAllBookmarks(): LiveData<Array<Menus>> { return appDbRepository.allBookMarks}
    fun addBookMark(rowId: Int){
        AppUtils.RunCodeAsync { appDbRepository.addBookMark(rowId = rowId)
        }.execute() }
    fun deleteBookMark(rowId: Int){
        AppUtils.RunCodeAsync{appDbRepository.deleteBookMark(rowId = rowId)
        }.execute()}
}
