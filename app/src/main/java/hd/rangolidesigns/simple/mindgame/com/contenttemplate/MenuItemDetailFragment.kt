package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.snackbar.Snackbar

import kotlinx.android.synthetic.main.fragment_item_detail.view.*


class MenuItemDetailFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        getViewModelFromMainActivity()
        val menuItemDetailView = inflateViewFromXML(inflater, container)
        mMenus=mainActivityViewModel.selectedMenus.toCollection(ArrayList())
        appCompatActivity = activity as AppCompatActivity
        initializeViewObjHolderVariables(menuItemDetailView!!)
        setupActionBar(menuItemDetailView)
        setupViewpagerAndCollapsingToolbar()
        return menuItemDetailView

    }

    private fun getViewModelFromMainActivity() {
        mainActivityViewModel = ViewModelProviders.of(activity as FragmentActivity).get(MainActivityViewModel::class.java)
    }
    private fun inflateViewFromXML(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View? {
        mLayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.fragment_item_detail, container, false)
    }
    private fun initializeViewObjHolderVariables(v: View) {
        toolbar = v.toolbar_holder
        collapsingToolbarLayout = v.collapsing_toolbar_layout
        itemDetailTextAdapter = v.vpItemDetailText
        itemToolbarImage = v.ivItemToolBarImage
    }
    private fun setupActionBar(menuItemDetailView: View?) {
        appCompatActivity.setSupportActionBar(menuItemDetailView!!.toolbar_holder)
        setHasOptionsMenu(true)
    }
    private fun setupViewpagerAndCollapsingToolbar() {
        setupViewPager()
        if (mMenus.isEmpty()) getDataFromDB()
        else {
            setupCollapsingToolBar()
            setupPageChangeListeners()
        }
    }
    private fun setupViewPager() {
        itemDetailTextAdapter.adapter = ItemDetailPagerAdapter(mItems = mMenus, mLayoutInflater = mLayoutInflater)
    }
    private fun getDataFromDB() {
        val selectedTopicID=mainActivityViewModel.menuID
        if (selectedTopicID!=0) {
            mainActivityViewModel.getMenus(selectedTopicID)
                .observe(activity as LifecycleOwner, Observer { menusList ->
                    mMenus = menusList.toCollection(ArrayList())
                    (itemDetailTextAdapter.adapter as ItemDetailPagerAdapter).setDataToPagerAdapter(mMenus)
                    setupCollapsingToolBar()
                    setupPageChangeListeners()

                })
        }
    }
    private fun setupCollapsingToolBar() {
        itemDetailTextAdapter.currentItem = getSavedPosition()
        collapsingToolbarLayout.title = mMenus[getSavedPosition()].name
        AppUtils.loadImageFromAssets(mMenus[getSavedPosition()].image, appCompatActivity, itemToolbarImage)
    }
    private fun setupPageChangeListeners() {
        itemDetailTextAdapter.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                updateMenuPosition(position)
                showInterstitialAd()
            }
            override fun onPageScrollStateChanged(state: Int) {
                loadNextItemFromViewpagerAdapter(appCompatActivity)
            }
        })
    }
    private fun showInterstitialAd() {
        AdObject.admob.loadNextScreen { }
    }
    private fun loadNextItemFromViewpagerAdapter(activity: AppCompatActivity) {
        updateMenuPosition(getPagerCurrentPosition())
        collapsingToolbarLayout.title = mMenus[getPagerCurrentPosition()].name
        AppUtils.loadImageFromAssets(
            mMenus[getPagerCurrentPosition()].image,
            activity,
            imageViewHolder = itemToolbarImage
        )
    }

    private fun getPagerCurrentPosition() = itemDetailTextAdapter.currentItem
    private fun updateMenuPosition(position: Int) {
        mainActivityViewModel.position = position
    }
    private fun getSavedPosition() = mainActivityViewModel.position


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val mnu = inflater.inflate(R.menu.menu, menu)

        setHasOptionsMenu(true)
        super.onCreateOptionsMenu(menu, inflater)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val itemSelected=mainActivityViewModel.selectedMenus[getPagerCurrentPosition()]
        when (item.itemId) {

        /*---SHARE THE ITEM----------------------------------*/
            R.id.btnShare -> {
//                AppUtils().shareImage(img_bmp,ctx = context as Context)
                AppUtils.shareText(itemSelected.name,itemSelected.description,activity as Activity)

            }

        /*----------------------BOOK MARK THE ITEM--------------------*/
            R.id.btnBookmark -> {
                mainActivityViewModel.addBookMark(itemSelected.id)
                Snackbar.make(activity!!.findViewById(R.id.clMainActivity), "Item-${itemSelected.name} saved successfully.", Snackbar.LENGTH_SHORT).show()
            }
        /*--LOAD THE BOOK MARKS SCREEN-----*/
            R.id.btnBookMarksList->{
                AdObject.admob.loadNextScreen {
                        appInterfaces.loadBookmarkItemlistScreen()
                }
            }
        /*------------RATE ME --------------*/
            R.id.btnRate->{
                AdObject.rateApp(activity as Context)
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppInterfaces){ appInterfaces = context }
    }

    lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private lateinit var mainActivityViewModel: MainActivityViewModel
    private lateinit var collapsingToolbarLayout: CollapsingToolbarLayout
    private lateinit var itemDetailTextAdapter: androidx.viewpager.widget.ViewPager
    private lateinit var itemToolbarImage:ImageView
    private lateinit var appInterfaces:AppInterfaces
    private lateinit var mLayoutInflater: LayoutInflater
    private lateinit var mMenus:ArrayList<Menus>
    private lateinit var appCompatActivity: AppCompatActivity

}