package hd.rangolidesigns.simple.mindgame.com.contenttemplate


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_header.view.*


/**
 * A simple [Fragment] subclass.
 * Use the [MenuItemListFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MenuItemListFragment : androidx.fragment.app.Fragment() {
    private lateinit var appInterfaces:AppInterfaces
    lateinit var itemListRecyclerView: RecyclerView
    private lateinit var mainActivityViewModel:MainActivityViewModel
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppInterfaces){ appInterfaces = context }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mainActivityViewModel= (activity as MainActivity).mainActivityViewModel!!
        val menuFragmentView: View = inflateFragmentXMLToView(inflater, container)
        setupRecyclerView(menuFragmentView)
        setDataToAdapter(menuFragmentView)
        return menuFragmentView
    }



    private fun inflateFragmentXMLToView(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View {
        val menuFragmentView: View = inflater.inflate(R.layout.fragment_item_list, container, false)
        return menuFragmentView
    }

    private fun setupRecyclerView(menuFragmentView: View) {
        menuFragmentView.pageHeader.text = "Menus loading.."
        itemListRecyclerView=menuFragmentView.findViewById(R.id.itemListRecyclerView)
        itemListRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ItemHeaderListAdapter(context, appInterfaces,AppUtils.ScreenName.MENU)
            addItemDecoration(ItemListViewDecoration(spacing = 10, includeEdge = false))
        }
    }
    private fun setDataToAdapter(topicFragmentView: View) {
        mainActivityViewModel.getMenus(mainActivityViewModel.topicID).observe(activity as LifecycleOwner, Observer {menusList->
            mainActivityViewModel.selectedMenus = menusList
            topicFragmentView.pageHeader.text = "${menusList.size} Menus loaded"
            (itemListRecyclerView.adapter as ItemHeaderListAdapter).setDataToItemHeaderListAdapter(menusList)
        })
    }



}
