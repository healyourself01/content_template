package hd.rangolidesigns.simple.mindgame.com.contenttemplate

class NoDuplicateStack {
    val stack: MutableList<String> = mutableListOf()
    val size: Int
        get() = stack.size

    // Push element onto the stack
    fun push(p: String) {
        val index = stack.indexOf(p)
        if (index != -1) {
            stack.removeAt(index)
        }
        stack.add(p)
    }

    // Pop upper element of stack
    fun pop(): String? {
        if (size > 0) {
            return stack.removeAt(stack.size - 1)
        } else {
            return null
        }
    }

    // Look at upper element of stack, don't pop it
    fun peek(): String? {
        if (size > 0) {
            return stack[stack.size - 1]
        } else {
            return null
        }
    }
}

