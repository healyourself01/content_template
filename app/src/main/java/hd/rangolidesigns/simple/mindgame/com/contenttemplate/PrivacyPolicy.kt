package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity

class PrivacyPolicy : AppCompatActivity() {
    private var mWebview: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mWebview = WebView(this)

        mWebview!!.settings.javaScriptEnabled = true // enable javascript

        val activity = this

        mWebview!!.webViewClient = WebViewClient()

        mWebview!!.loadUrl("http://mindgamesoft.in/privacy.html")
        setContentView(mWebview)

    }


}
