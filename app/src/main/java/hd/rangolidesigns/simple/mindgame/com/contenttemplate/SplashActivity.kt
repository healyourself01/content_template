package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_splash.*
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {
    private val interstitialAd= InterstitialAd(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        AdObject.connectivityManager =
                applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (AdmobUtility.showAdOrNot()) {
            interstitialAd.adUnitId=getString(R.string.INTERSTITIAL_ID)
            loadInterstitialAd()
            setupInterstitialAdListeners()
        }else gotoMainActivity()

    }

    private fun loadInterstitialAd() {
        if (Worker.isConnectedToWifiOrMobileNetwork()) {
            interstitialAd.loadAd(AdRequest.Builder().build())
        }
        else gotoMainActivity()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        gotoMainActivity()
    }

    private fun setupInterstitialAdListeners() {
        interstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
                AdmobUtility.saveAdLoadedTime()
                interstitialAd.show()
            }
            override fun onAdFailedToLoad(errorCode: Int) {
                gotoMainActivity()
            }
            override fun onAdLeftApplication() {
                gotoMainActivity()
            }
            override fun onAdClosed() {
                gotoMainActivity()
            }
        }
    }

    private fun gotoMainActivity() {
        this.finish() //Remove the Splash from back stack
        AdmobUtility.saveAdLoadedTime()
        startActivity(Intent(this,MainActivity::class.java))
    }


}
