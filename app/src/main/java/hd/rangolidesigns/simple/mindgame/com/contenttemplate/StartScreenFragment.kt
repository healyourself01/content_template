package hd.rangolidesigns.simple.mindgame.com.contenttemplate


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_start_screnn.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StartScrenn.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class StartScreenFragment : androidx.fragment.app.Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var appInterfaces:AppInterfaces

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the rate_me_layout for this fragment
        val v = inflater.inflate(R.layout.fragment_start_screnn, container, false)

        v.btnGallery.setOnClickListener(
                View.OnClickListener {
                    AdObject.admob.loadNextScreen { appInterfaces.loadTopicsScreen() }
                }
        )
        v.btnRateMe.setOnClickListener(View.OnClickListener {
            AdObject.rateApp(context as Context)
        })

        v.tvPrivacy.setOnClickListener {
            appInterfaces.loadPrivacyPolicy()
        }

        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppInterfaces){ appInterfaces = context }
    }
}
