package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
class TeluguTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {

        applyCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        applyCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {

        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        val customFont = AdObject.typeface
        typeface = customFont
    }
}