package hd.rangolidesigns.simple.mindgame.com.contenttemplate


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_header.view.*


/**
 * A simple [Fragment] subclass.
 * Use the [TopicFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class
TopicFragment : androidx.fragment.app.Fragment() {
    private lateinit var appInterfaces:AppInterfaces
    lateinit var itemListRecyclerView:RecyclerView
    private lateinit var mainActivityViewModel:MainActivityViewModel
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppInterfaces){ appInterfaces = context }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mainActivityViewModel= (activity as MainActivity).mainActivityViewModel!!
        val topicFragmentView: View = inflateFragmentXMLToView(inflater, container)
        setupRecyclerView(topicFragmentView)
        setDataToAdapter(topicFragmentView)

        return topicFragmentView
    }

    private fun inflateFragmentXMLToView(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): View {
        val topicFragmentView: View = inflater.inflate(R.layout.fragment_item_list, container, false)
        return topicFragmentView
    }

    private fun setupRecyclerView(topicFragmentView: View) {
        topicFragmentView.pageHeader.text = "Categories loading.."
        itemListRecyclerView=topicFragmentView.findViewById(R.id.itemListRecyclerView)
        itemListRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ItemHeaderListAdapter(context, appInterfaces,AppUtils.ScreenName.TOPIC)
            addItemDecoration(ItemListViewDecoration(spacing = 10, includeEdge = false))
        }
    }
    private fun setDataToAdapter(topicFragmentView: View) {
        mainActivityViewModel.allTopics.observe(activity as AppCompatActivity as LifecycleOwner, Observer {
            val allTopics=it
            topicFragmentView.pageHeader.text = "${allTopics.size} Categories loaded"
            (itemListRecyclerView.adapter as ItemHeaderListAdapter).setDataToItemHeaderListAdapter(allTopics)
        })
    }


}
