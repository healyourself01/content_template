package hd.rangolidesigns.simple.mindgame.com.contenttemplate

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.JobIntentService
import java.io.IOException
import java.util.*

/**
 * Created by Gokul on 2/25/2018.
 */

class Worker : JobIntentService() {
    /**
     * Result receiver object to send results
     */

    @SuppressLint("DefaultLocale", "RestrictedApi")
    override fun onHandleWork(intent: Intent) {
        Log.d(TAG, "onHandleWork() called with: intent = [$intent]")
        val timerObj = Timer()
        val timerTaskObj = object : TimerTask() {
            override fun run() {
                //perform your action here
                AdObject.IsOnline.postValue(isInternetAvailable())
            }
        }
        timerObj.schedule(timerTaskObj, 0, 10000)
    }

    companion object {
        private val TAG = "Worker"
        /**
         * Unique job ID for this service.
         */
        internal val DOWNLOAD_JOB_ID = 1000
        /**
         * Actions download
         */
        private val ACTION_DOWNLOAD = "action.DOWNLOAD_DATA"

        /**
         * Convenience method for enqueuing work in to this service.
         */
        fun enqueueWork(context: Context) {
            val intent = Intent(context, Worker::class.java)
            JobIntentService.enqueueWork(context, Worker::class.java, DOWNLOAD_JOB_ID, intent)
        }
        fun isConnectedToWifiOrMobileNetwork(): Boolean {

            return if (AdObject.connectivityManager.activeNetworkInfo != null) {
                AdObject.connectivityManager.activeNetworkInfo!!.isConnectedOrConnecting
            } else {
                false;
            }
        }

    }

    fun isInternetAvailable():Boolean {

        if (!isConnectedToWifiOrMobileNetwork()){
            return false
        }
        try {
            /*Pinging to Google server*/
            val command = "ping -c 1 google.com"
            val result = Runtime.getRuntime().exec(command).waitFor() == 0
            return result

        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e:InterruptedException) {
            e.printStackTrace()
        } finally {

        }
        return false
    }



}